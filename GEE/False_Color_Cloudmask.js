// ** Function to apply mask and add quality bands to Landsat 8 images
var addQualityBands = function(image, bands) {
  return maskClouds(image)
	.multiply(0.0000275).add(-0.2)
	.addBands(image.metadata('system:time_start'))
	.addBands(image.normalizedDifference(['SR_B5', 'SR_B4']).rename('NDVI'));
};


// ** Function to mask clouds in Landsat 8 imagery
var maskClouds = function(image) {
  var cloudDilBitMask = 1 << 1; 
  var cirrusBitMask = 1 << 2;
  var cloudsBitMask = 1 << 3;
  var cloudShadowBitMask = 1 << 4;   
  var qa = image.select('QA_PIXEL');  
  var mask = qa.bitwiseAnd(cloudShadowBitMask).eq(0)
	.and(qa.bitwiseAnd(cloudsBitMask).eq(0))
	.and(qa.bitwiseAnd(cloudDilBitMask).eq(0))
	.and(qa.bitwiseAnd(cirrusBitMask).eq(0));  
  return image.updateMask(mask); 
}




var landsat = ee.ImageCollection('LANDSAT/LC08/C02/T1_L2').filterDate('2017-06-01', '2017-09-30').filterBounds(UMBC)


var Median = landsat.reduce('median').multiply(0.0000275).add(-0.2);  // this last bit converts the SR whole integer number (used for storage by USGS) to the correct 0-1 SR double number you were expecting.
print('Median',Median)
Map.addLayer(Median, {bands: ['SR_B4_median', 'SR_B3_median', 'SR_B2_median'], min: 0, max: 0.5}, 'Median Raw');



var collection_m = landsat.map(addQualityBands);


var Median_m = collection_m.reduce('median');
print('Median_m',Median_m)
Map.addLayer(Median_m, {bands: ['SR_B5_median', 'SR_B4_median', 'SR_B3_median'], min: 0, max: 0.5}, 'Median Filtered');


var recentValueComposite = collection_m.qualityMosaic('system:time_start');
print(recentValueComposite)
Map.addLayer(recentValueComposite, {bands: ['SR_B5', 'SR_B4', 'SR_B3'], min: 0, max: 0.5}, 'Recent value composite')



var NDVIPixelComposite = collection_m.qualityMosaic('NDVI');
print(NDVIPixelComposite)
Map.addLayer(NDVIPixelComposite, {bands: ['NDVI'], min: 0, max: 0.5, palette: ['blue','red', 'yellow','green']}, 'Greenest pixel (NDVI) composite');
Map.addLayer(NDVIPixelComposite, {bands: ['SR_B4', 'SR_B5', 'SR_B3'], min: 0, max: 0.5}, '(RGB)Greenest pixel (NDVI) composite');
