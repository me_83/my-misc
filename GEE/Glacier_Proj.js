// GLACIER NATIONAL PARK //
var dataset = ee.ImageCollection('MODIS/061/MOD10A1')
                  .filter(ee.Filter.date('2020-06-01', '2020-09-01'))
                  .map(function(image){return image.clip(glacial_nps)});
var snowCover = dataset.select('NDSI_Snow_Cover');
var snowCoverVis = {
  min: 0.0,
  max: 100.0,
  palette: ['black', '0dffff', '0524ff', 'ffffff'],
};
Map.setCenter(-113.8000, 48.7550, 9);
Map.addLayer(snowCover, snowCoverVis, 'Glacier Park Glacial Area 2020');

var dataset1 = ee.ImageCollection('MODIS/061/MOD10A1')
                  .filter(ee.Filter.date('2000-06-01', '2000-09-01'))
                  .map(function(image){return image.clip(glacial_nps)});
var snowCover = dataset1.select('NDSI_Snow_Cover');
var snowCoverVis = {
  min: 0.0,
  max: 100.0,
  palette: ['black', '0dffff', '0524ff', 'ffffff'],
};
Map.setCenter(-113.8000, 48.7550, 9);
Map.addLayer(snowCover, snowCoverVis, 'Glacier Park Glacial Area 2000');




// ALL NATIONAL PARK SERVICE //
var dataset2 = ee.ImageCollection('MODIS/061/MOD10A1')
                  .filter(ee.Filter.date('2020-06-01', '2020-09-01'))
                  .map(function(image){return image.clip(nps_boundaries)});
var snowCover = dataset2.select('NDSI_Snow_Cover');
var snowCoverVis = {
  min: 0.0,
  max: 100.0,
  palette: ['black', '0dffff', '0524ff', 'ffffff'],
};
Map.setCenter(-113.8000, 48.7550, 9);
Map.addLayer(snowCover, snowCoverVis, 'NPS Total Glacial Area 2020');

var dataset3 = ee.ImageCollection('MODIS/061/MOD10A1')
                  .filter(ee.Filter.date('2000-06-01', '2000-09-01'))
                  .map(function(image){return image.clip(nps_boundaries)});
var snowCover = dataset3.select('NDSI_Snow_Cover');
var snowCoverVis = {
  min: 0.0,
  max: 100.0,
  palette: ['black', '0dffff', '0524ff', 'ffffff'],
};
Map.setCenter(-113.8000, 48.7550, 9);
Map.addLayer(snowCover, snowCoverVis, 'NPS Total Glacial Area 2000');

//EXPORT CODE
var exportOptions = {
  region: glacial_nps,
  scale: 500,
  folder: 'GEE_Output',  // Specify the folder in your Google Drive you want it to go to
  maxPixels: 1e13
};
Export.image.toDrive({
  image: dataset2020.median(),  
  description: 'Glacier_Park_Glacial_Area_2020',
  region: exportOptions.region,
  scale: exportOptions.scale,
  folder: exportOptions.folder,
  maxPixels: exportOptions.maxPixels
});
Export.image.toDrive({
  image: dataset2010.median(),
  description: 'Glacier_Park_Glacial_Area_2010',
  region: exportOptions.region,
  scale: exportOptions.scale,
  folder: exportOptions.folder,
  maxPixels: exportOptions.maxPixels
});
Export.image.toDrive({
  image: dataset2000.median(),
  description: 'Glacier_Park_Glacial_Area_2000',
  region: exportOptions.region,
  scale: exportOptions.scale,
  folder: exportOptions.folder,
  maxPixels: exportOptions.maxPixels
});

Export.table.toDrive({
  collection: glacial_nps,
  description:'Glacial_NPS_Poly',
  fileFormat: 'SHP',
  folder: 'GEE_Output'
});