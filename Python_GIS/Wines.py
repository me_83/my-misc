import numpy as np

reds = np.genfromtxt("winequality-red.csv", delimiter=";", skip_header=1)
whites = np.genfromtxt("winequality-white.csv", delimiter=";", skip_header=1)
wcitric_comp = whites[:,3] > (whites[:,2]*3)
cwhites = whites[wcitric_comp,:]
rcitric_comp = reds[:,3] > (reds[:,2]*3)
creds= reds[rcitric_comp,:]
qreds=creds[(creds[:,11]>5)]
qwhites=cwhites[(cwhites[:,11]>5)]
sreds = np.array((qreds[:,7]*10)-qreds[:,10])
swhites = np.array((qwhites[:,7]*10)-qwhites[:,10])

if np.min(sreds)<np.min(swhites):
    print ("The best is red wine {}".format(qreds[np.argmin(sreds)]))
elif np.min(sreds)>np.min(swhites):
    print("The best is white wine {}".format(qwhites[np.argmin(swhites)]))
else:
    if qreds[np.argmin(sreds),3]<qwhites[np.argmin(swhites),3]:
        print("The best is red wine {}".format(qreds[np.argmin(sreds)]))
    elif qreds[np.argmin(sreds),3]>qwhites[np.argmin(swhites),3]:
        print("The best is white wine {}".format(qwhites[np.argmin(swhites)]))
    else:
        print("This is a hard choice. I really cannot tell")