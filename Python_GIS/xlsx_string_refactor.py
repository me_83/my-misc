if __name__ == '__main__':
    import pandas as pd
    tas = pd.read_excel('CT Taser Data.xlsx', sheet_name='2015 Taser Incidents')
    print(tas['Law Enforcement Agency'], tas['Race'])
    print()
    tas_a = tas.filter(items=['Law Enforcement Agency', 'Race'])
    # New DataFrame Object !NEEDS TO BE RESIZED!
    dmy_df = (tas_a[['Law Enforcement Agency']].assign(**pd.get_dummies(tas_a[['Race']])).groupby(['Law Enforcement Agency']))
    print(dmy_df)
    # FORMATS DATAFRAME OBJECT WHILE SUMMING INFORMATION, THEN RENAMES COLUMNS
    new_df = dmy_df.size().to_frame().join(dmy_df.sum()).reset_index().rename(columns={0: 'T_Count','Race_Asian': 'Asian', 'Race_Black': 'Black', 'Race_Unknown': 'Unknown', 'Race_White': 'White'})
    print(new_df)
    #new_df.to_csv('Comp Taser Data')