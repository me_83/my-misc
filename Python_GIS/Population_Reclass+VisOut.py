if __name__ == '__main__':
    import pandas as pd
    import geopandas as gp
    import matplotlib.pyplot as plt

    def pop_reclass(num):
        if num < 100000:
            return 'small'
        elif (100000 <= num <= 1000000):
            return 'medium'
        elif (num > 1000000):
            return 'large'

    def pop_marker(str):
        if str == 'large':
            return int(10)
        elif str == 'medium':
            return int(5)
        elif str == 'small':
            return int(1)

    naturalearth_lowres = gp.read_file('naturalearth_lowres')
    CA = naturalearth_lowres[naturalearth_lowres.name == 'United States of America']
    cadf = pd.read_csv(r'C:\Users\ccal4\PycharmProjects\GES400\CACities.csv')

    cadf = pd.DataFrame(cadf)
    print(cadf)
    cadf['pop_level'] = cadf['population'].apply(pop_reclass)
    cadf = gp.GeoDataFrame(cadf, geometry=gp.points_from_xy(cadf.lon, cadf.lat), crs=naturalearth_lowres.crs)
    print(cadf)
    print(cadf.columns)

    mark_val = []
    for i in range(len(cadf)):
        mark_val.append(pop_marker(cadf['pop_level'][i])*3)

    fig, ax = plt.subplots()
    # ax.set(xlim=(-124.7,-113.9), ylim=(32.3,42.2 ))
    CA.plot(ax=ax, color='grey', edgecolor='black')
    cadf.plot(ax=ax, column='pop_level', markersize=mark_val)
    plt.show()
    #out = r"C:\Users\ccal4\PycharmProjects\GES400\CA_city_size.shp"
    #cadf.to_file(out)
